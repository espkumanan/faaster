# Faaster - Quick and easy FaaS micro-services

- [Faaster - Quick and easy FaaS micro-services](#faaster---quick-and-easy-faas-micro-services)
    - [About](#about)
    - [Getting Started With Example](#getting-started-with-example)
        - [Pre-conditions](#pre-conditions)
        - [Getting Ready](#getting-ready)
        - [Initialize](#initialize)
        - [Check the plan](#check-the-plan)
        - [Apply the plan](#apply-the-plan)
        - [Cleanup (Undo) the plan](#cleanup-undo-the-plan)
    - [Getting Started](#getting-started)
        - [Install via `git` sub-modules](#install-via-git-sub-modules)
        - [Protecting Credentials](#protecting-credentials)
    - [Faaster Modules](#faaster-modules)
        - [AWS](#aws)
            - [API](#api)
            - [CRUD](#crud)
            - [Other](#other)
        - [Diagram](#diagram)
    - [Pattern Examples](#pattern-examples)
        - [Separate Methods and Common Role](#separate-methods-and-common-role)
        - [Separate Methods and Role](#separate-methods-and-role)
        - [Authorizing Method Requests](#authorizing-method-requests)

## About

**Faaster** is a set of `terraform` modules that make it faster to deploy "serverless" microservices using Amazon's Lambda, API Gateway, Dynamo DB and S3 services (AWS).

Lambda functions can be implemented using the following language platforms.

* Node.js using `fn_lang="node"` (or not, since this is the default)
* Go using `fn_lang="go"` but ...
    > Terraform has a bug in its `archive_file` resource wherethe ZIP files it makes do not preserve the executable bit for binaries which in turn results in Lambda refusing (rightly) to execute the Go-based handlers.

## Example

The example has been moved to it's own repository: [faaster-example](https://gitlab.com/nogginly/faaster-example)

## Getting Started

### Install via `git` sub-modules

Define your application project folder structure as follows.

```
+-- myapp
    +-- deployments/                <-- where your deployment targets go
    |   +-- deploy_target/
    |       +-- credentials         <-- AWS API access credentials file
    |       +-- terraform.tfvars    <-- Deployment-specific properties
    +-- deps/
    |   +-- faaster/                <-- pull here as a submodule
    +-- modules/
    |   +-- faaster/                 <-- symlink to 'myapp/deps/faaster/modules`
    +-- functions/
    |   +-- node/                   <-- your Node.js functions go under here
    |   |   +-- one_GET/
    |   |   +-- one_POST/
    |   +-- go/                     <-- you Go functions go under here
    |       +-- two_GET/
    |       +-- two_DELETE/
    +-- main.tf                     <-- your main `terraform` config
    +-- README.md
    +-- CHANGELOG.md
    +-- LICENSE
```

Using `git submodule` pull in the `faaster` repo under the `deps/` folder and then symlink the `deps/faaster/modules` as `modules/faaster` so that `faaster` modules show up in your projects `modules/` folder where `terraform` will look for modules that you use.

### Protecting Credentials

> When developing "serverless" app using a `git` repo, it's important to make sure you don't check-in credentials and other important stuff into a public repo somewhere. Take a look at the `.gitignore` file in this repo and feel free to use it as a starting point. It will keep the usual places where creds are put as well as runtime data files etc out of your app's repo.

## Faaster Modules

> From this point on there will be docs on the modules and how they work and patterns. Work in progress.

### AWS
#### API

| Module                     | Inputs Required                                                              | Optional Inputs                                                                                                                         | Outputs                                             |
| -------------------------- | ---------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- |
| `aws/api`                  | `deploy_prefix`, `api_name`, `api_desc`, `binary_media_types`                |                                                                                                                                         | `id, root_id`                                       |
| `aws/api_resource`         | `api_id`, `path_name`, `parent_id`                                           |                                                                                                                                         | `id`                                                |
| `aws/api_lambda_role`      | `deploy_prefix`, `name`                                                      | [`opt_cwlogging`]                                                                                                                       | `role_id`, `role_arn`                               |
| `aws/api_lambda_method_fn` | `deploy_prefix`, `fn_name`, `http_method`, `api_id`, `res_id`, `lambda_role` | [`fn_class`], [`fn_lang`], [`auth_id`], [`fn_mem`], [`key_arn`], [`env_vars`]                                                           | `type`, `version`                                   |
| `aws/api_lambda_method`    | `deploy_prefix`, `fn_name`, `http_method`, `api_id`, `res_id`                | [`fn_class`], [`fn_lang`], [`opt_cwlogging`], [`auth_id`], [`fn_mem`], [`key_arn`], [`env_vars`]                                        | `type`, `version`, `role_id`, `role_arn`            |
| `aws/api_lambda_auth_fn`   | `deploy_prefix`, `fn_name`, `api_id`, `lambda_role`                          | [`fn_class`], [`fn_lang`], [`auth_ttl`], [`fn_mem`], [`key_arn`], [`env_vars`]                                                          | `type`, `version`, `auth_id`                        |
| `aws/api_lambda_auth`      | `deploy_prefix`, `fn_name`, `api_id`, `lambda_role`                          | [`fn_class`], [`fn_lang`], [`auth_ttl`], [`opt_cwlogging`], [`fn_mem`], [`key_arn`], [`env_vars`]                                       | `type`, `version`, `role_id`, `role_arn`, `auth_id` |
| `aws/api_deploy`           | `deploy_prefix`, `name`, `api_id`, `method_types`                            | [`domain_name`], [`domain_prefix`], [`opt_throttling_burst_limit`], [`opt_throttling_rate_limit`], [`opt_cwlogging`], [`opt_cwmetrics`] | `raw_url`, `url`, `description`                     |

#### CRUD

| Module                   | Inputs                                                                                                                                                                                         | Outputs |
| ------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------- |
| `aws/crud_dynamo_access` | `table_name`, `table_arn`, `read_allow_count`, `read_allow_roles`, `write_allow_count`, `write_allow_roles`, `delete_allow_count`, `delete_allow_roles`                                        | n/a     |
| `aws/crud_s3_access`     | `bucket_name`, `bucket_arn`, `bucket_prefix`, `read_allow_count`, `read_allow_roles`, `write_allow_count`, `write_allow_roles`, `delete_allow_count`, `delete_allow_roles`                     | n/a     |
| `aws/crud_es_access`     | `elasticsearch_name`, `elasticsearch_arn`, `elasticsearch_index`, `read_allow_count`, `read_allow_roles`, `write_allow_count`, `write_allow_roles`, `delete_allow_count`, `delete_allow_roles` | n/a     |

#### Other

| Module               | Inputs Required                          | Optional Inputs                                            | Outputs                                                                                 |
| -------------------- | ---------------------------------------- | ---------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| `aws/cron_lambda_fn` | `deploy_prefix`, `fn_name`, `sched_expr` | `fn_source`, `fn_lang`, `fn_class`, `fn_mem`, `fn_timeout` | `role_id`, `role_arn`                                                                   |
| `aws/lambda_fn`      | `deploy_prefix`, `fn_name`,              | `fn_source`, `fn_lang`, `fn_class`, `fn_mem`, `fn_timeout` | `version`, `arn`, `invoke_arn`, `last_modified`, `source_code_hash`, `source_code_size` |

### Diagram

The following diagram shows the relationship between the modules that allows an API to be defined and deployed.

![Faaster Model Diagram](diagrams/faaster_model.png)

> I wish there was a way to avoid the semantically unncessary but syntactically required linking between `api_lambda_method` and the `api->id` output.

## Pattern Examples

### Separate Methods and Common Role

Here we define an "Example" API with a `/msg` resource associated with `POST` and `GET` methods implemented using two AWS Lambda functions via `api_lambda_method_fn`. In this case we have a common `api_lambda_role` which is associated with both the Lambda methods, thus they both have the same access to resources.

![Faaster Example Diagram with Common Role](diagrams/faaster_example.png)

### Separate Methods and Role

Here we use `api_lambda_method` to define each method with its own IAM role. This enables each method to have its own role, which in turn allows different access policies to be associated with each method. While the same could be done by explicitly using the `api_lambda_method` and `api_lambda_role` modules, using the combined "method and role" module keeps the main config simpler.

![Faaster Example Diagram with Per-method Roles](diagrams/faaster_example_2.png)

### Authorizing Method Requests

_Coming soon._

