# FaaSter Changelog

The latest version of this file can be found at the master branch of the `nogginly/faaster` repository.

## 0.9.2

* Cleaned up README to remove example docs and add pointer to example repo.

## 0.9.1

* Example has been relocated to `nogginly/faaster-example` repo.

## 0.9.0

* Modified `api_lambda_*` modules to use `lambda_fn` as underlying module. This in turn has a small breaking change: The `class` property is replaced by `fn_class`. Sorry for the inconvenience.

## 0.8.3

* Added optional `fn_source` property for `faaster/aws/lambda_fn` and `faaster/aws/cron_lambda_fn` modules to allow using same source to drive multiple Lambda functions.

## 0.8.2

* Fixed `faaster/aws/cron_lambda_fn` to pass through the `fn_timeout` value to underlying `lambda_fn` module. 
* Updated test `04_cron_lambda` to set `fn_timeout` to verify pass through.

## 0.8.1

* Adding simple `faaster/aws/lambda_fn` module for future us as base module with common behaviour for packaging
* Adding `faaster/aws/cron_lambda_fn` module that can be used to defined scheduled execution of Lambda functions.

## 0.8.0

* Adding `bin/faasterraform.sh` to help manage multiple separate deployments with separate credentials and variable configurations.
* Tweak to `faaster/aws/api_deploy` outputs to publish custom domain URL (if configured) and raw URL.
* Example modified.

## 0.7.3

* Modified all the `api_lambda_*` modules to use `node.js 8.10` as the `node` runtime for Lambda functions.

## 0.7.2

* Modified all the `api_lambda_*` modules to support optional `class` parameter that allows for an infix path between `functions/` and the language folder name. See issue #1 for details.

## 0.7.1 (master)

* Modified `aws/api_deploy` to look for API gateway domain cert in `us-east-1` region, since underlying CloudFront system only works with certs from `us-east-1`.

## 0.7.0 (master)

* Namespace change moving all AWS-based `faaster` modules under `aws/` sub-folder. 

## 0.6.0 (master)

* Adding `crud_es` module to allow mapping access contro to Elastic Search instances via AWS.
* Adding API gateway rate/bursth throttle control options to `api_deploy` module.
* Separating configurable option to enable/disable CloudWatch metrics for API gateway methods

## 0.5.0 (master)

* Getting ready for public distribution under Mozilla Public License 2.0.
* Adding `example/` folder
* Updated `README.md` with TOC and sections on what it is and how to get started.
* Added ability to set memory size and KMS key to Lambda modules.
* Added ability to get version as output from Lambda modules.
* Added optional ability to attach custom domain name (by combining `deploy_prefix` with `domain_name`) to an API deployment (edge optimize mode only)
* Added optional environment variables to Lambda functions and methods.

## 0.4.0 (master)

* Modified all `api_lambda_*` modules to support both `node.js` and `golang` functions.
    * Added `fn_lang` parameter, which supports `node` (default) and `go` values.
    * Modified where functions are located to look in a language-specific sub-folder under `functions/` using the `fn_lang` value.
* Modified all tests to put their `node.js` handlers under `functions/node/` folder.
* The `archive_file` resource in Terraform's `archive` provider doesn't respect file access flags and therefore doesn't mark an executable properly, which in turn causes Lambda to not execute the functions packaged into zip files.
    * The overall behaviour for `fn_lang` works. But until the `archive_file` for Go won't work until Terraform fixes the bug!
    * Sigh.

## 0.3.0 (master)

* Added test case `03_msg_AUTH_GET` to test API gateway authorizer
* Added role self-contained `api_lambda_auth` authorizer
* Modified `api_lambda_method_fn`
    * Added optional `auth_id` input var which can be used to associate an authorizer with method
* Added role-independent `api_lambda_auth_fn` authorizer
* Renamed role-independent `api_lambda_method` to `api_lambda_method_fn`
* Renamed role self-contained `api_lambda_method_role` to `api_lambda_method`

## 0.2.0 (master)

* Added `crud_s3_access` module
    * this sets up R/W/D access policies for a given S3 bucket and links them with supplied roles.
    * ideal for setting up different CRUD access policies for different Lambda methods with their own roles.
* Added `crud_dynamo_access` module - this sets up R/W/D access policies for a given Dynamo table and links them with supplied roles. - ideal for setting up different CRUD access policies for different Lambda methods with their own roles.

## 0.1.0 (master)

* Added `api_lambda_method_role` module
    * this sets up a Lambda method with its own IAM role
    * in anticipation of wanting Lambda methods with isolated policies so that we can manage access to other AWS resources with finer grain control (e.g. a method can modify a bucket, and another can only read from same bucket, enforced via role/policy.)
* Added `deploy_prefix` module input
    * to all modules except `api_resource` which doesn't give me an attribute where I can put the prefix.
    * `api_lambda_method` sets the deployment prefix as the `DEPLOY_PREFIX` environment variable on its Lambda function
* Added `api_lambda_role` module
    * this sets up a lambda execution role for use by lambda methods.
    * need this to be able to attach additional policies to a method to allow access to other services (e.g. Dynamo DB table)
    * includes policy to allow Cloud Watch logging
    * supports `opt_cwlogging` input to enable Lambda CW logging (default is "no")
* Modified `api_lambda_method`
    * to use AWS provider data sources to obtain account ID and AWS region, removing the `aws_region` module parameter.
    * to add source ARN to `aws_lambda_permission` resource so that access to the lambda is locked down the API resource and the method integration's HTTP method for the account and API.
    * to change output `type` value to contain HTTP method and resource path (e.g. "GET/message")
    * to accept `lambda_role` input based on the `api_lambda_role` output
* Modified `api_deploy`
    * to add output `description` which concatenates the specified input API method types, making for a clear list of deployed methods.
    * supports `opt_cwlogging` input to enable APIG CW logging (default is "no")
        * _This depends on setting APIG CW logging IAM role ARN manually which doesn't work for my account._
* Modified `api`
    * to support `binary_media_types` attribute, which defaults to `[ "*/*" ]` to allow all binary response types
* Modified tests
    * to add the lambda function `node.js` code.
    * to work with changes in above module parameters
    * to add `api_list` output to export the `api_deploy.description` attribute so we can see the hooked up methods
    * to add a route parameter-based path resource to handle `GET /message/{id}`
    * to set "test01" `deploy_prefix` as input for all modules that support it
