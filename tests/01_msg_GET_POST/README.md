
## The Inside View

The following diagrams shows the internal mapping of each module to the `terraform` AWS resources that are used to implement the modules.

```mermaid
graph LR;

    subgraph api
        aws_api_gateway_rest_api --- root_resource
    end

    subgraph api_resource_A
        aws_api_gateway_resource_A --> aws_api_gateway_rest_api
        aws_api_gateway_resource_A --> root_resource
    end

    subgraph api_resource_A_id
        aws_api_gateway_resource_A_id --> aws_api_gateway_rest_api
        aws_api_gateway_resource_A_id --> aws_api_gateway_resource_A
    end

    subgraph code
        function_A_POST>functions/A_POST/]
        function_A_GET>functions/A_GET/]
    end

    subgraph api_lambda_role
        aws_iam_role
    end

    subgraph api_lambda_method_A_POST
        archive_file_1 --> function_A_POST
        aws_lambda_function_1 --> archive_file_1
        aws_lambda_function_1 --> aws_iam_role
        aws_lambda_permission_1 --> aws_lambda_function_1
        aws_api_gateway_method_1 --> aws_api_gateway_rest_api
        aws_api_gateway_method_1 --> aws_api_gateway_resource_A
        aws_api_gateway_integration_1 --> aws_api_gateway_rest_api
        aws_api_gateway_integration_1 --> aws_api_gateway_resource_A
        aws_api_gateway_integration_1 --> aws_lambda_function_1
        aws_api_gateway_integration_1 --> aws_api_gateway_method_1
    end

    subgraph api_lambda_method_A_GET
        archive_file_2 --> function_A_GET
        aws_lambda_function_2 --> aws_iam_role
        aws_lambda_function_2 --> archive_file_2
        aws_lambda_permission_2 --> aws_lambda_function_2
        aws_api_gateway_method_2 --> aws_api_gateway_rest_api
        aws_api_gateway_method_2 --> aws_api_gateway_resource_A_id
        aws_api_gateway_integration_2 --> aws_api_gateway_rest_api
        aws_api_gateway_integration_2 --> aws_api_gateway_resource_A_id
        aws_api_gateway_integration_2 --> aws_lambda_function_2
        aws_api_gateway_integration_2 --> aws_api_gateway_method_2
    end

    subgraph api_deploy
        aws_api_gateway_deployment -. type .-> aws_api_gateway_integration_1
        aws_api_gateway_deployment -. type .-> aws_api_gateway_integration_2
        aws_api_gateway_deployment -- id --> aws_api_gateway_rest_api
    end
    
```