locals {
  fn_base_path = "${var.fn_class}${var.fn_class == "" ? "" : "/"}${var.fn_lang}${var.fn_lang == "go" ? "/src" : ""}"

  // Convert class to a safe name for use as part of the name of the function ZIP file by
  //    - replacing all '/' with '_'
  //    - appending '_' if class is not empty
  //
  class_as_name = "${replace(var.fn_class,"/","_")}${var.fn_class=="" ? "" : "_"}"

  // Determine the name of the Lambda source folder, taking into account override via `fn_source` property.
  source_name = "${var.fn_source == "" ? var.fn_name : var.fn_source}"

  // Regardless of source name, always use `fn_name` for the ZIP file per Lambda so we can keep them separate
  zip_name = "${var.fn_name}"

  default_env_vars = {
    DEPLOY_PREFIX = "${var.deploy_prefix}"
  }
}

data "archive_file" "lambda" {
  type        = "zip"
  source_dir  = "./functions/${local.fn_base_path}/${local.source_name}"
  output_path = "./functions/fun_${local.class_as_name}${local.zip_name}.zip" # Make ZIP files using `fn_name`.
}

resource "aws_lambda_function" "lambda" {
  function_name    = "${var.deploy_prefix}-${var.fn_name}-lambda"
  handler          = "${var.fn_lang == "node" ? "index.handler" : "handler"}"
  runtime          = "${var.fn_lang == "node" ? "nodejs8.10" : "go1.x"}"
  memory_size      = "${var.fn_mem}"
  filename         = "${data.archive_file.lambda.output_path}"
  source_code_hash = "${data.archive_file.lambda.output_base64sha256}"
  role             = "${var.lambda_role}"
  timeout          = "${var.fn_timeout}"
  kms_key_arn      = "${var.key_arn}"

  environment {
    variables = "${merge(var.env_vars,local.default_env_vars)}"
  }
}
