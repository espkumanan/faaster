#
# This is out here because of dependencies without which TF tries
# to make this prematurely
resource "aws_api_gateway_deployment" "deploy" {
  rest_api_id       = "${var.api_id}"
  stage_name        = "${var.name}"
  stage_description = "${var.deploy_prefix} deployment stage ${var.name}"
  description       = "${join(", ",var.method_types)}"
}

#
# Enable logging and metrics for all methods in this deployment/stage
#
resource "aws_api_gateway_method_settings" "deploy" {
  rest_api_id = "${aws_api_gateway_deployment.deploy.rest_api_id}"
  stage_name  = "${aws_api_gateway_deployment.deploy.stage_name}"
  method_path = "*/*"

  settings {
    metrics_enabled        = "${var.opt_cwmetrics == "yes"}"
    logging_level          = "${var.opt_cwlogging == "yes" ? "INFO" : "OFF" }"
    throttling_rate_limit  = "${var.opt_throttling_rate_limit}"
    throttling_burst_limit = "${var.opt_throttling_burst_limit}"
  }
}

locals {
  // if specified, use the domain name prefix as a sub-domain.
  deploy_fqdn = "${var.domain_prefix == "" ? var.domain_name : "${var.domain_prefix}.${var.domain_name}"}"
  cert_domain = "${var.domain_prefix == "" ? var.domain_name : "*.${var.domain_name}"}"
}

// We need to make sure we have this cert in `us-east-1` for use with CloudFront.
provider "aws" {
  alias  = "us-east-1"
  region = "us-east-1"
}

data "aws_acm_certificate" "deploy" {
  provider = "aws.us-east-1"
  count    = "${var.domain_name == "" ? 0 : 1}"
  domain   = "${local.cert_domain}"
  statuses = ["ISSUED"]
}

resource "aws_api_gateway_domain_name" "deploy" {
  count           = "${var.domain_name == "" ? 0 : 1}"
  domain_name     = "${local.deploy_fqdn}"
  certificate_arn = "${data.aws_acm_certificate.deploy.arn}"
}

#
# If domain name specified, then map it to the deployment stage
#
data "aws_route53_zone" "deploy" {
  count = "${var.domain_name == "" ? 0 : 1}"
  name  = "${var.domain_name}"
}

resource "aws_route53_record" "deploy" {
  count   = "${var.domain_name == "" ? 0 : 1}"
  zone_id = "${data.aws_route53_zone.deploy.id}"
  name    = "${aws_api_gateway_domain_name.deploy.domain_name}"
  type    = "A"

  alias = {
    name                   = "${aws_api_gateway_domain_name.deploy.cloudfront_domain_name}"
    zone_id                = "${aws_api_gateway_domain_name.deploy.cloudfront_zone_id}"
    evaluate_target_health = false
  }
}

resource "aws_api_gateway_base_path_mapping" "deploy" {
  count       = "${var.domain_name == "" ? 0 : 1}"
  api_id      = "${var.api_id}"
  stage_name  = "${aws_api_gateway_deployment.deploy.stage_name}"
  base_path   = "${aws_api_gateway_deployment.deploy.stage_name}"
  domain_name = "${aws_route53_record.deploy.fqdn}"
}
