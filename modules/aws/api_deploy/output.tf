output "url" {
  description = "The API URL, based on the custom domain name if specified."
  value       = "${var.domain_name == "" ? aws_api_gateway_deployment.deploy.invoke_url : "https://${local.deploy_fqdn}/${aws_api_gateway_deployment.deploy.stage_name}"}"
}

output "raw_url" {
  description = "The raw API URL associated with the deployment."
  value       = "${aws_api_gateway_deployment.deploy.invoke_url}"
}

output "description" {
  description = "A description of the linked API methods associated with this deployment."
  value       = "${aws_api_gateway_deployment.deploy.description}"
}
