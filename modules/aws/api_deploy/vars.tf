variable "method_types" {
  type        = "list"
  description = "List of API method integration types that will be deployed here."
}

variable "api_id" {
  description = "The ID of the API Gateway REST API that is deployed."
}

variable "name" {
  description = "The name of the deployment stage"
}

variable "deploy_prefix" {
  description = "A prefix that is used with AWS resource names to distinguish between different deployments. Use alphanumeric characters and `-` only."
}

variable "opt_cwlogging" {
  description = "Set this attribute to 'yes' to include Cloud Watch logging of API gateway methods."
  default     = "no"
}

variable "opt_cwmetrics" {
  description = "Set this attribute to 'yes' to enable Cloud Watch Metrics by API gateway methods."
  default     = "no"
}

variable "opt_throttling_rate_limit" {
  description = "Set this to a number to specify the maximum number of requests per second to allow. Default is 1000."
  default     = "1000"
}

variable "opt_throttling_burst_limit" {
  description = "Set this to a number to specify the maximum burst requests to allow. Default is 250."
  default     = "250"
}

variable "domain_name" {
  description = "Set this attribute to the name of domain to map the API deployment to. The domain must already be available as a Route53 hosted zone, and an AWS Certficate **in `us-east-1` region** must be available for the domain name. Default is no domain."
  default     = ""
}

variable "domain_prefix" {
  description = "Set this to specify a prefix to the domain name set via `domain_name`. Do not include a terminating period (.) as module will append that if this property is set. And if set we will look for a wildcard certificate **in `us-east-1` region**. Default is no prefix."
  default     = ""
}
