// Policies

// DELETE
data "aws_iam_policy_document" "es_delete_access" {
  count = "${var.delete_allow_count}"

  statement {
    effect = "Allow"

    actions = [
      "es:ESHttpDelete",
    ]

    resources = [
      "${var.elasticsearch_arn}/${var.elasticsearch_index}*",
    ]
  }
}

resource aws_iam_policy "es_delete_access" {
  count  = "${var.delete_allow_count}"
  name   = "DeleteIn-${var.elasticsearch_name}"
  policy = "${data.aws_iam_policy_document.es_delete_access.json}"
}

// WRITE
data "aws_iam_policy_document" "es_write_access" {
  count = "${var.write_allow_count}"

  statement {
    effect = "Allow"

    actions = [
      "es:ESHttpPost",
      "es:ESHttpPut",
    ]

    resources = [
      "${var.elasticsearch_arn}/${var.elasticsearch_index}*",
    ]
  }
}

resource aws_iam_policy "es_write_access" {
  count  = "${var.write_allow_count}"
  name   = "WriteTo-${var.elasticsearch_name}"
  policy = "${data.aws_iam_policy_document.es_write_access.json}"
}

// READ/QUERY
data "aws_iam_policy_document" "es_read_access" {
  count = "${var.read_allow_count}"

  statement {
    effect = "Allow"

    actions = [
      "es:ESHttpGet",
      "es:ESHttpHead",
    ]

    resources = [
      "${var.elasticsearch_arn}/${var.elasticsearch_index}*",
    ]
  }
}

resource aws_iam_policy "es_read_access" {
  count  = "${var.read_allow_count}"
  name   = "ReadFrom-${var.elasticsearch_name}"
  policy = "${data.aws_iam_policy_document.es_read_access.json}"
}

// -------- Attach policy to Lambda methods by iterating through the lists and creating
//          attachment resources. 

resource aws_iam_role_policy_attachment "es_read_access" {
  count      = "${var.read_allow_count}"
  role       = "${element(var.read_allow_roles,count.index)}"
  policy_arn = "${aws_iam_policy.es_read_access.arn}"
}

resource aws_iam_role_policy_attachment "es_write_access" {
  count      = "${var.write_allow_count}"
  role       = "${element(var.write_allow_roles,count.index)}"
  policy_arn = "${aws_iam_policy.es_write_access.arn}"
}

resource aws_iam_role_policy_attachment "es_delete_access" {
  count      = "${var.delete_allow_count}"
  role       = "${element(var.delete_allow_roles,count.index)}"
  policy_arn = "${aws_iam_policy.es_delete_access.arn}"
}
